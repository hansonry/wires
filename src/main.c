#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <SDL.h>

struct point
{
   int x;
   int y;
};

#define GRID_SPACING 50

int main(int argc, char * args[])
{
   SDL_Window * window;
   SDL_Renderer * rend;
   SDL_Event event;
   
   int x, y;
   int scale;
   float grid_space;
   int screen_width, screen_height;
   struct point screen;
   
   scale = 0;
   screen.x = 0;
   screen.y = 0;
   
   bool running;
   
   SDL_Init(SDL_INIT_EVERYTHING);
   window = SDL_CreateWindow("Wires", SDL_WINDOWPOS_CENTERED, 
                                      SDL_WINDOWPOS_CENTERED,
                                      640, 480, SDL_WINDOW_RESIZABLE);
   rend = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
   
   running = true;
   while(running)
   {
      while(SDL_PollEvent(&event))
      {
         if(event.type == SDL_QUIT)
         {
            running = false;
         }
         else if(event.type == SDL_MOUSEMOTION)
         {
            if((event.motion.state & SDL_BUTTON_RMASK) == SDL_BUTTON_RMASK)
            {
               screen.x += event.motion.xrel;
               screen.y += event.motion.yrel;
            }               
         }
         else if(event.type == SDL_MOUSEWHEEL)
         {
            if(event.wheel.direction == SDL_MOUSEWHEEL_NORMAL)
            {
               scale -= event.wheel.y;
            }
            else
            {
               scale += event.wheel.y;
            }
            
            if(scale < 0)
            {
               scale = 0;
            }
         }
      }
      
      SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
      SDL_RenderClear(rend);
      
      SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
      SDL_RenderDrawPoint(rend, 10, 10);
      SDL_RenderDrawLine(rend, 20, 20, 30, 30);
      
      
      SDL_GetRendererOutputSize(rend, &screen_width, &screen_height);
      
      grid_space = (pow(10, scale / 10) * GRID_SPACING) / pow(1.1, scale);
   
      for(y = 0; y < (screen_height / grid_space) + 2; y++)
      {
         for(x = 0; x < (screen_width / grid_space) + 2; x++)
         {
            SDL_RenderDrawPoint(rend, 
                                (screen.x % (int)grid_space) + (x * grid_space), 
                                (screen.y % (int)grid_space) + (y * grid_space));
         }
      }
      
      
      SDL_RenderPresent(rend);
   }
   
   SDL_DestroyRenderer(rend);
   SDL_DestroyWindow(window);
   SDL_Quit();
   printf("End\n");
   return 0;
}
