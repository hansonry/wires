settings = NewSettings()

if family == "windows" then
   settings.cc.includes:Add("libs/SDL2-2.0.10/include")
   settings.link.flags:Add("-Xlinker /SUBSYSTEM:CONSOLE")
   settings.link.libpath:Add("libs/SDL2-2.0.10/lib/x64")   
   settings.link.libs:Add("SDL2")
   settings.link.libs:Add("SDL2main")
else
   settings.cc.flags:Add("`sdl2-config --cflags`")
   settings.link.flags:Add("`sdl2-config --libs`")
end 

source = Collect("src/*.c")
objects = Compile(settings, source)
exe = Link(settings, "wires", objects)

if family == "windows" then
   AddDependency(exe, CopyFile("SDL2.dll",   "libs/SDL2-2.0.10/lib/x64/SDL2.dll"))
end
